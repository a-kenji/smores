# smores
[![Crates](https://img.shields.io/crates/v/smores?style=flat-square)](https://crates.io/crates/smores)
[![Documentation](https://img.shields.io/badge/smores-documentation-fc0060?style=flat-square)](https://docs.rs/smores)
[![Matrix Chat Room](https://img.shields.io/badge/chat-on%20matrix-1d7e64?logo=matrix&style=flat-square)](https://matrix.to/#/#smores-main:matrix.org)

A `smos` helper cli.

Currently `smores single --help` is a supported action.

## Chat Room
Join our matrix chat room, for possibly synchronous communication.

## Contributing
We welcome contributions from the community! If you're interested in contributing to smores, please refer to the contribution guidelines for instructions on how to get started.

[How to contribute.](./docs/CONTRIBUTING.md)

## Changes
[Changelog](./CHANGELOG.md)

## License
MIT
