use clap::Parser;

use self::{
    cli::CliArgs,
    smos::{SmosDocument, Value},
};
use crate::smos::Properties;

mod cli;
mod smos;

fn main() -> anyhow::Result<()> {
    let args = CliArgs::parse();

    match args.subcommand() {
        cli::Command::Single {
            content,
            file: _,
            url,
            header,
        } => {
            let mut doc = SmosDocument::default();
            let mut value = Value::default();
            let mut properties = Properties::default();
            if let Some(url) = url {
                properties.url = url.into();
            }
            value.contents = content.clone();
            if let Some(header) = header {
                value.header = header.into();
            }
            value.properties = Some(properties);
            doc.value = Some(vec![value]);
            let out = serde_yaml::to_string(&doc).unwrap();
            println!("{}", out);
        }
    }

    Ok(())
}
