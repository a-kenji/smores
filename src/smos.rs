use serde::{Deserialize, Serialize};

const SMOS_DOC_VERSION: &str = "2.0.0";

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct SmosDocument {
    pub version: String,
    pub value: Option<Vec<Value>>,
}

impl Default for SmosDocument {
    fn default() -> Self {
        Self {
            version: SMOS_DOC_VERSION.into(),
            value: None,
        }
    }
}

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Value {
    pub header: String,
    pub contents: Option<String>,
    pub properties: Option<Properties>,
    #[serde(rename = "state-history")]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub state_history: Option<Vec<StateHistory>>,
}

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Properties {
    pub url: String,
}

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct StateHistory {
    pub state: String,
    pub time: String,
}

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Value2 {
    pub entry: Entry,
    pub forest: Vec<Forest>,
}

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Entry {
    pub header: String,
    #[serde(rename = "state-history")]
    pub state_history: Vec<StateHistory2>,
}

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct StateHistory2 {
    pub state: String,
    pub time: String,
}

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Forest {
    pub header: String,
    #[serde(rename = "state-history")]
    pub state_history: Vec<StateHistory3>,
}

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct StateHistory3 {
    pub state: String,
    pub time: String,
}

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Value3 {
    pub header: String,
    #[serde(rename = "state-history")]
    pub state_history: Vec<StateHistory4>,
}

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct StateHistory4 {
    pub state: String,
    pub time: String,
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn minimal_doc() {
        let stream = r#"
        version: 2.0.0
        "#;
        let _doc: SmosDocument = serde_yaml::from_str(stream).unwrap();
    }
    #[test]
    fn header() {
        let stream = r#"
        version: 2.0.0
        value:
        - header: This is not a test!
        "#;
        let doc: SmosDocument = serde_yaml::from_str(stream).unwrap();
        let value = doc.value.unwrap().first().unwrap().clone();
        let header = value.header;
        assert_eq!("This is not a test!", header);
    }
    #[test]
    fn content() {
        let stream = r#"
        version: 2.0.0
        value:
        - header: This is not a test!
          contents: |
            This is valuable content.
        "#;
        let doc: SmosDocument = serde_yaml::from_str(stream).unwrap();
        let value = doc.value.unwrap().first().unwrap().clone();
        let contents = value.contents.unwrap();
        assert_eq!("This is valuable content.\n", contents);
    }
    #[test]
    fn properties_url() {
        let stream = r#"
        version: 2.0.0
        value:
        - header: This is not a test!
          contents: |
            This is valuable content.
          properties:
            url: http3-explained.haxx.se/
        "#;
        let doc: SmosDocument = serde_yaml::from_str(stream).unwrap();
        let value = doc.value.unwrap().first().unwrap().clone();
        let properties = value.properties.unwrap();
        assert_eq!("http3-explained.haxx.se/", properties.url);
    }
}
