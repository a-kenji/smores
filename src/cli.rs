use std::fmt::Display;

use clap::{Parser, Subcommand};

#[derive(Parser, Debug)]
#[command(author, version = CliArgs::unstable_version(), about, long_about = None)]
#[command(name = "smores")]
#[command(next_line_help = true)]
pub struct CliArgs {
    #[command(subcommand)]
    subcommand: Command,
}

#[allow(unused)]
impl CliArgs {
    /// Surface current version together with the current git revision and date, if available
    fn unstable_version() -> &'static str {
        const VERSION: &str = env!("CARGO_PKG_VERSION");
        let date = option_env!("GIT_DATE").unwrap_or("no_date");
        let rev = option_env!("GIT_REV").unwrap_or("no_rev");
        // This is a memory leak, only use sparingly.
        Box::leak(format!("{VERSION} - {date} - {rev}").into_boxed_str())
    }

    pub(crate) fn subcommand(&self) -> &Command {
        &self.subcommand
    }
}

#[derive(Subcommand, Debug)]
pub(crate) enum Command {
    /// Add a new Smos record
    #[clap(alias = "a")]
    #[command(arg_required_else_help = true)]
    Single {
        header: Option<String>,
        #[arg(long, short)]
        content: Option<String>,
        /// The target file
        #[arg(long, short)]
        file: Option<String>,
        /// Sets the url property
        #[arg(long, short)]
        url: Option<String>,
    },
}
